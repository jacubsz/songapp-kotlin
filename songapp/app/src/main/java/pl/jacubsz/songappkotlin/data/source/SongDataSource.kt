package pl.jacubsz.songappkotlin.data.source

import io.reactivex.Observable
import pl.jacubsz.songappkotlin.data.model.Song

/**
 * SongDataSource Contract
 * Created by Jakub on 2018-04-25.
 */
interface SongDataSource {

    fun getSongs(searchPhrase: String, resultsOffset: Int, resultsLimit: Int): Observable<List<Song>>

    fun getSongDetails(songId: Long): Observable<Song>
}