package pl.jacubsz.songappkotlin

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import pl.jacubsz.songappkotlin.di.DaggerSongApplicationComponent

/**
 * SongApplication
 * Created by Jakub on 2018-04-25.
 */
class SongApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerSongApplicationComponent.builder().create(this)
    }

}