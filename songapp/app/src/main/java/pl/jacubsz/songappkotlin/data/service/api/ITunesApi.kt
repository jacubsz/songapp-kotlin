package pl.jacubsz.songappkotlin.data.service.api

import io.reactivex.Observable
import pl.jacubsz.songappkotlin.data.model.ITunesResponseWrapper
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * ITunes API definition
 * Created by Jakub on 2018-04-25.
 */
interface ITunesApi {

    @GET("search")
    fun searchSongs(@Query("term") searchPhrase: String, @Query("offset") offset: Int, @Query("limit") limit: Int): Observable<ITunesResponseWrapper>

    @GET("lookup")
    fun getSongById(@Query("id") songId: Long): Observable<ITunesResponseWrapper>
}