package pl.jacubsz.songappkotlin.data.usecase.songdetails

import pl.jacubsz.songappkotlin.data.model.Song
import pl.jacubsz.songappkotlin.data.usecase.UseCase

/**
 * SongDetails Use Case
 * Created by Jakub on 2018-04-25.
 */
class SongDetails(val songId: Long) : UseCase<Song>