package pl.jacubsz.songappkotlin.di

import com.google.gson.*
import dagger.Module
import dagger.Provides
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat
import java.util.*
import javax.inject.Singleton

/**
 * Gson DI Module
 * Created by Jakub on 2018-04-25.
 */
@Module
class GsonModule {

    private val DATETIME_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .withLocale(Locale.getDefault())

    @Singleton
    @Provides
    fun providesGson(): Gson =
            GsonBuilder()
                    //DateTime serializer
                    .registerTypeAdapter(DateTime::class.java,
                            JsonSerializer<DateTime> { src, _, _ ->
                                JsonPrimitive(src.withZone(DateTimeZone.UTC).toString(DATETIME_FORMATTER))
                            })
                    .registerTypeAdapter(DateTime::class.java,
                            JsonDeserializer<DateTime> { json, _, _ ->
                                val dateTime = DateTime(json.asString, DateTimeZone.UTC)
                                dateTime.withZone(DateTimeZone.getDefault())
                            })
                    .create()

}