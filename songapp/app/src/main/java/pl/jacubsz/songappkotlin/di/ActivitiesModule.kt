package pl.jacubsz.songappkotlin.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import pl.jacubsz.songappkotlin.MainActivity

/**
 * Activities DI Module
 * Created by Jakub on 2018-04-25.
 */
@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

}