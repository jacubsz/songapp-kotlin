package pl.jacubsz.songappkotlin.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import pl.jacubsz.songappkotlin.SongApplication
import javax.inject.Singleton

/**
 * Song Application main DI component
 * Created by Jakub on 2018-04-25.
 */
@Singleton
@Component(modules = arrayOf(
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivitiesModule::class,
        GsonModule::class,
        NetModule::class,
        ApiModule::class
))
interface SongApplicationComponent : AndroidInjector<SongApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SongApplication>()
}