package pl.jacubsz.songappkotlin.data.usecase.songlist

import io.reactivex.Observable
import pl.jacubsz.songappkotlin.data.model.Song
import pl.jacubsz.songappkotlin.data.service.api.ITunesApi
import pl.jacubsz.songappkotlin.data.usecase.UseCaseExecutor
import javax.inject.Inject

/**
 * SongListExecutor
 * Created by Jakub on 2018-04-25.
 */
class SongListApiExecutor @Inject constructor(
        private val iTunesApi: ITunesApi
) : UseCaseExecutor<List<Song>, SongList> {

    override fun execute(useCase: SongList): Observable<List<Song>> {
        return iTunesApi.searchSongs(useCase.searchPhrase, useCase.offset, useCase.limit)
                .map { it.results }
    }
}