package pl.jacubsz.songappkotlin.di

import android.util.Log
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.jacubsz.songappkotlin.BuildConfig
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Net DI Module
 * Created by Jakub on 2018-04-25.
 */
@Module
class NetModule {

    @Provides
    @Singleton
    fun provideRequestLoggingInterceptor(): HttpLoggingInterceptor =
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("HttpRequest", it) })
                    .setLevel(HttpLoggingInterceptor.Level.BODY)

    @Provides
    @Singleton
    fun provideOkHttpClient(
            loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient =
            OkHttpClient.Builder()
                    .addInterceptor(loggingInterceptor)
                    .build()

    @Provides
    @Singleton
    fun provideAuthRetrofit(
            okHttpClient: OkHttpClient,
            gson: Gson
    ): Retrofit =
            Retrofit.Builder()
                    .baseUrl(BuildConfig.HOST)
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()

}