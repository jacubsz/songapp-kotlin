package pl.jacubsz.songappkotlin.data.model

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime

/**
 * iTunes response item data model
 * Created by Jakub on 2018-04-25.
 */
data class Song(
        @SerializedName("trackId")
        var id: Long = 0,

        @SerializedName(value = "trackName", alternate = arrayOf("Song Clean"))
        var title: String? = null,

        @SerializedName(value = "artistName", alternate = arrayOf("ARTIST CLEAN"))
        var artistName: String? = null,

        @SerializedName("artworkUrl100")
        var artworkUrl: String? = null,

        var collectionName: String? = null,
        var trackTimeMillis: Long = 0,
        var primaryGenreName: String? = null,
        var releaseDate: DateTime? = null,
        val releaseYear: Int = 0
)