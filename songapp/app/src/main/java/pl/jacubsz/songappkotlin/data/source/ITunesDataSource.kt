package pl.jacubsz.songappkotlin.data.source

import io.reactivex.Observable
import pl.jacubsz.songappkotlin.data.model.Song
import pl.jacubsz.songappkotlin.data.usecase.songdetails.SongDetails
import pl.jacubsz.songappkotlin.data.usecase.songdetails.SongDetailsApiExecutor
import pl.jacubsz.songappkotlin.data.usecase.songlist.SongList
import pl.jacubsz.songappkotlin.data.usecase.songlist.SongListApiExecutor
import javax.inject.Inject

/**
 * ITunesDataSource
 * Created by Jakub on 2018-04-25.
 */
class ITunesDataSource @Inject constructor(
        private val songDetails: SongDetailsApiExecutor,
        private val songList: SongListApiExecutor
) : SongDataSource {

    override fun getSongs(searchPhrase: String, resultsOffset: Int, resultsLimit: Int): Observable<List<Song>>
            = songList.execute(SongList(searchPhrase, resultsOffset, resultsLimit))

    override fun getSongDetails(songId: Long): Observable<Song>
            = songDetails.execute(SongDetails(songId))


}