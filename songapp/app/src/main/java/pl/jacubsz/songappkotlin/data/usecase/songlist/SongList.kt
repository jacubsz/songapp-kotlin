package pl.jacubsz.songappkotlin.data.usecase.songlist

import pl.jacubsz.songappkotlin.data.model.Song
import pl.jacubsz.songappkotlin.data.usecase.UseCase

/**
 * SongList Use Case
 * Created by Jakub on 2018-04-25.
 */
class SongList(
        val searchPhrase: String,
        val offset: Int,
        val limit: Int
) : UseCase<List<Song>>