package pl.jacubsz.songappkotlin

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import pl.jacubsz.songappkotlin.data.repository.SongRepository
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var songRepository: SongRepository //test purposes only

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        songRepository.getSongs("a", 0, 10).subscribe() //test pursposes only
    }
}
