package pl.jacubsz.songappkotlin.data.repository

import io.reactivex.Observable
import pl.jacubsz.songappkotlin.data.model.Song
import pl.jacubsz.songappkotlin.data.source.ITunesDataSource
import pl.jacubsz.songappkotlin.data.source.SongDataSource
import javax.inject.Inject

/**
 * SongRepository with two possible sources: API / Local storage
 * Created by Jakub on 2018-04-25.
 */
class SongRepository @Inject constructor(
        private val iTunesDataSource: ITunesDataSource
) : SongDataSource {

    override fun getSongs(searchPhrase: String, resultsOffset: Int, resultsLimit: Int): Observable<List<Song>> {
        return iTunesDataSource.getSongs(searchPhrase, resultsOffset, resultsLimit)
    }

    override fun getSongDetails(songId: Long): Observable<Song> {
        return iTunesDataSource.getSongDetails(songId)
    }

}
