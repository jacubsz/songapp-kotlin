package pl.jacubsz.songappkotlin.data.usecase.songdetails

import io.reactivex.Observable
import pl.jacubsz.songappkotlin.data.model.Song
import pl.jacubsz.songappkotlin.data.service.api.ITunesApi
import pl.jacubsz.songappkotlin.data.usecase.UseCaseExecutor
import javax.inject.Inject

/**
 * SongDetailsApiExecutor
 * Created by Jakub on 2018-04-25.
 */
class SongDetailsApiExecutor @Inject constructor(
        private val iTunesApi: ITunesApi
) : UseCaseExecutor<Song, SongDetails> {

    override fun execute(useCase: SongDetails): Observable<Song> {
        return iTunesApi.getSongById(useCase.songId)
                .map { it.results[0] }
    }

}