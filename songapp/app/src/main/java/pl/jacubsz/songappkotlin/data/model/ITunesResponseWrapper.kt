package pl.jacubsz.songappkotlin.data.model

/**
 * iTunes request response data wrapper
 * Created by Jakub on 2018-04-25.
 */
data class ITunesResponseWrapper(
        val resultCount: Int,
        val results: List<Song>
)