package pl.jacubsz.songappkotlin.di

import dagger.Module
import dagger.Provides
import pl.jacubsz.songappkotlin.data.service.api.ITunesApi
import retrofit2.Retrofit

/**
 * Api DI Module
 * Created by Jakub on 2018-04-25.
 */
@Module
class ApiModule {

    @Provides
    fun provideITunesApi(retrofit: Retrofit): ITunesApi = retrofit.create(ITunesApi::class.java)

}