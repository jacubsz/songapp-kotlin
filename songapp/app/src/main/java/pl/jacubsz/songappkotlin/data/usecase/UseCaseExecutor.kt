package pl.jacubsz.songappkotlin.data.usecase

import io.reactivex.Observable

/**
 * UseCaseExecutor interface
 * Created by Jakub on 2018-04-25.
 */
interface UseCaseExecutor<T, in U : UseCase<T>> {
    fun execute(useCase: U): Observable<T>
}