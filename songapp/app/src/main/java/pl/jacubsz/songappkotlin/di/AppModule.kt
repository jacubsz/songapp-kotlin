package pl.jacubsz.songappkotlin.di

import android.content.Context
import dagger.Module
import dagger.Provides
import pl.jacubsz.songappkotlin.SongApplication

/**
 * App general DI Module
 * Created by Jakub on 2018-04-25.
 */
@Module
class AppModule {

    @Provides
    fun provideContext(application: SongApplication): Context = application.applicationContext

}